import math

def sphereVolume(r):
    v = (4 / 3) * math.pi * math.pow(r, 3)
    return v

def sphereSurface(r):
    v = 4 * math.pi * math.pow(r, 2)
    return v

def cylinderVolume(r, h):
    v = math.pi * math.pow(r, 2) * h
    return v

def cylinderSurface(r, h):
    v = 2 * math.pi * r * h + 2 * math.pi * math.pow(r, 2)
    return v

def coneVolume(r, h):
    v = math.pi * math.pow(r, 2) * (h / 3)
    return v

def coneSurface(r, h):
    v = math.pi * r + (r + math.sqrt(math.pow(r, 2) + math.pow(h, 2)))
    return v
