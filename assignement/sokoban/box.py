class Box:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.underBox = ' '

    def getPos(self):
        return self.x, self.y

    def setPos(self, x, y):
        self.x = x
        self.y = y

    def canMove(self, map):
        tmp = []
        if map[self.y - 1][self.x] != '#' and map[self.y - 1][self.x] != 'X':
            tmp.append(0);
        else:
            tmp.append(1);
        if map[self.y + 1][self.x] != '#' and map[self.y + 1][self.x] != 'X':
            tmp.append(0);
        else:
            tmp.append(1);
        if map[self.y][self.x + 1] != '#' and map[self.y][self.x + 1] != 'X':
            tmp.append(0);
        else:
            tmp.append(1);
        if map[self.y][self.x - 1] != '#' and map[self.y][self.x - 1] != 'X':
            tmp.append(0);
        else:
            tmp.append(1);
        return tmp

    def move(self, dir):
        if dir == 0:
            self.y = self.y - 1
        elif dir == 1:
            self.y = self.y + 1
        elif dir == 2:
            self.x = self.x + 1
        elif dir == 3:
            self.x = self.x - 1

    def setUnderBox(self, map):
        self.underBox = map[self.y][self.x]

    def getUnderBox(self):
        return self.underBox
