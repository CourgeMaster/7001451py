from box import Box
from platform import system

class Map:
    def __init__(self, rawData):
        self.rawData = rawData;
        self.height = len(rawData)
        self.width = len(rawData[0])
        self.basePlayerPos = []
        self.boxList = []
        self.targetList = []
        i = 0;
        while i != len(rawData):
            j = 0
            while j != len(rawData[i]):
                if rawData[i][j] == 'P':
                    self.basePlayerPos.append(i)
                    self.basePlayerPos.append(j)
                elif rawData[i][j] == 'X':
                    self.boxList.append(Box(j, i))
                elif rawData[i][j] == 'O':
                    self.targetList.append([j, i])
                j = j + 1
            i = i + 1

    def hasWin(self):
        count = 0
        i = 0
        while i != len(self.boxList):
            j = 0
            while j != len(self.targetList):
                if self.boxList[i].getPos()[0] == self.targetList[j][0] and self.boxList[i].getPos()[1] == self.targetList[j][1]:
                    count = count  + 1
                j = j + 1
            i = i + 1
        if count == len(self.targetList):
            return True
        return False

    def setMap(self, rd):
        self.rawData;

    def getMap(self):
        return self.rawData;

    def printMap(self, stdscr, term):
        i = 0
        term.update()
        while i != len(self.rawData):
            if system() == 'Windows':
                stdscr.addstr(int(int(term.getTerminalSize()[1]) / 2) - int(self.height / 2) + i, int(int(term.getTerminalSize()[0]) / 2 - int(self.width / 2)), self.rawData[i] + "\n")
            else:
                stdscr.addstr(int(int(term.getTerminalSize()[0]) / 2) - int(self.height / 2) + i, int(int(term.getTerminalSize()[1]) / 2 - int(self.width / 2)), self.rawData[i] + "\n")
            i = i + 1
        stdscr.refresh()

    def getBox(self, x, y):
        tuple = [x, y]
        i = 0
        while i != len(self.boxList):
            if self.boxList[i].getPos()[0] == tuple[0] and self.boxList[i].getPos()[1] == tuple[1]:
                return self.boxList[i]
            i = i + 1
        return self.boxList[0]

    def getPlayerPos(self):
        return self.basePlayerPos
