'''
Created on 11 . 2019

@author: Robin LOMON
'''

class ConfigManager:
    def getInfo(self):
        #openning a text file with previous players informations
        settings_r = open("./.config","r")
        #creating a dictionnary with all informations from the text file
        player_dict = {}
        line=settings_r.readlines()
        j=0
        player_name=""
        for i in range(len(line)):
            j=0
            while line[i][j]!='|':
                player_name+=line[i][j]
                j+=1
            j+=1 #directly going to the highscore in the file
            player_highscore=line [i][j]
            player_dict[player_name]=player_highscore
            player_name=""
        return player_dict

    def writeInfo(self,info):
        settings = open("./.config","w")
        for a in info:
            settings.write(a)
            settings.write("|")
            settings.write(info[a])
            settings.write("\n")
