#coding:cp1252
#!/usr/bin/env python
from commandManager import CommandManager
from platform import system
import os
import sys

PYNAME = "main.py"

def main():
    if system() == 'Windows' and len(sys.argv) == 1:
        os.system("start cmd.exe @cmd /k " + PYNAME + " cmd")
        exit(0)
    cmd = CommandManager()
    cmd.commandLoop()
    print("Goodbye!")

main()
