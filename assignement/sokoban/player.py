from utils import replacer

class Player:
    def __init__(self, position):
        self.PosY = position[0]
        self.PosX = position[1]
        self.underPlayer = ' '
        self.gotX = False

    def movePlayer(self, mapObj, stdscr):
        key = stdscr.getch()
        map = mapObj.getMap()
        if key == ord('z') and self.canGo(self.PosX, self.PosY - 1, mapObj, 0) == True:
            self.PosY = self.PosY - 1
        elif key == ord('s') and self.canGo(self.PosX, self.PosY + 1, mapObj, 1) == True:
            self.PosY = self.PosY + 1
        elif key == ord('d') and self.canGo(self.PosX + 1, self.PosY, mapObj, 2) == True:
            self.PosX = self.PosX + 1
        elif key == ord('q') and self.canGo(self.PosX - 1, self.PosY, mapObj, 3) == True:
            self.PosX = self.PosX - 1
        else:
            return key
        if self.gotX == False:
            self.underPlayer = map[self.PosY][self.PosX]
        map[self.PosY] = replacer(map[self.PosY], 'P', self.PosX)
        mapObj.setMap(map)
        self.gotX = False
        return key

    def getPlayerPositions(self):
        positions = []
        positions.append(self.PosX)
        positions.append(self.PosY)
        return positions

    def canGo(self, x, y, mapObj, dir):
        map = mapObj.getMap()
        if map[y][x] == '#':
            self.gotX = True
            return False
        elif map[y][x] == 'X':
            box = mapObj.getBox(x, y)
            tmp = box.canMove(map)
            if tmp[dir] == 1:
                return False
            else:
                self.gotX = True
                box.move(dir)
                map[self.PosY] = replacer(map[self.PosY], self.underPlayer, self.PosX)
                self.underPlayer = box.getUnderBox()
                box.setUnderBox(map)
                boxPos = box.getPos()
                map[boxPos[1]] = replacer(map[boxPos[1]], 'X', boxPos[0])
        else:
            self.gotX = False
            map[self.PosY] = replacer(map[self.PosY], self.underPlayer, self.PosX)
        return True
