from os import walk

class MapManager:
    def __init__(self, path):
        self.path = path
        self.mapList = []
        for (dirpath, dirnames, filenames) in walk(self.path):
            self.mapList.extend(filenames)
            break
        self.mapList.sort()

    def printMaps(self):
        for name in self.mapList:
            print(name)

    def changePath(self):
        self.path = input("Enter the exact path to the new map folder: ")
        self.mapList.clear()
        for (dirpath, dirnames, filenames) in walk(self.path):
            self.mapList.extend(filenames)
            break
        self.mapList.sort()

    def getPath(self):
        return self.path

    def getMapList(self):
        return self.mapList
