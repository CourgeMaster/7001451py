from terminalManager import TerminalManager
from map import Map
from player import Player
import curses
import sys

class GameManager:
    def __init__(self, name, mapList, mapIndex, mapPath):
        self.name = name
        self.mapPath = mapPath
        self.title = self.fileToTab("./assets/mainMenu")
        self.playButton = self.fileToTab("./assets/playButton")
        self.quitButton = self.fileToTab("./assets/quitButton")
        self.cursor = self.fileToTab("./assets/cursor")
        self.curentMap = mapIndex
        self.stdscr = curses.initscr()
        self.term = TerminalManager()
        self.stdscr.keypad(1)
        self.mapList = mapList
        curses.noecho()
        curses.cbreak()
        curses.curs_set(0)

    def fileToTab(self, path):
        tmp = []
        with open(path, 'r+') as f:
            lines = f.readlines()
            for i in range(0, len(lines)):
                line = lines[i]
                tmp.append(line)
        return tmp

    def printMenu(self, cursor):
            i = 0
            while i != len(self.title):
                self.stdscr.addstr(i, 0, self.title[i] + "\n")
                i = i + 1
            j = 0
            while j != len(self.playButton):
                if cursor == 0:
                    self.stdscr.addstr(5 + j + i, 27, self.playButton[j] + "\n", curses.A_STANDOUT)
                else:
                    self.stdscr.addstr(5 + j + i, 27, self.playButton[j] + "\n")
                j = j + 1
            k = 0
            while k != len(self.quitButton):
                if cursor == 1:
                    self.stdscr.addstr(5 + k + j + i, 27, self.quitButton[k] + "\n", curses.A_STANDOUT)
                else:
                    self.stdscr.addstr(5 + k + j + i, 27, self.quitButton[k] + "\n")
                k = k + 1
            self.stdscr.refresh()

    def mainMenu(self):
        key = ''
        cursor = 0
        self.printMenu(cursor)
        while key != ord('p'):
            key = self.stdscr.getch()
            if key == ord('z'):
                cursor = cursor - 1
            elif key == ord('s'):
                cursor = cursor + 1
            if cursor < 0:
                cursor = 0
            elif cursor > 1:
                cursor = 1
            self.printMenu(cursor)
        return cursor

    def loadMap(self):
        self.rawMap = self.fileToTab(self.mapPath + "/" + self.mapList[self.curentMap])
        self.map = Map(self.rawMap)
        self.player = Player(self.map.getPlayerPos())

    def resetTerm(self):
        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.curs_set(1)
        curses.endwin()

    def changeMap(self):
        key = ''
        self.curentMap = self.curentMap  + 1
        if self.curentMap >= len(self.mapList):
            key = 'p'
        else:
            self.loadMap()
        return key

    def gameLoop(self):
        cursor = self.mainMenu()
        self.term.clearTerminal(self.stdscr)
        if cursor == 0:
            self.loadMap()
            self.stdscr.refresh()
            self.map.printMap(self.stdscr, self.term)
            key = ''
            while key != ord('p'):
                self.term.update()
                self.map.printMap(self.stdscr, self.term)
                key = self.player.movePlayer(self.map, self.stdscr)
                if self.map.hasWin() == True and self.changeMap() == 'p':
                    break
                if key == ord('r'):
                    self.loadMap()
                self.term.clearTerminal(self.stdscr)
            self.gameLoop()
        return self.curentMap
