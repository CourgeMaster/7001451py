import os
from platform import system
import struct
import shutil

class TerminalManager:
    def __init__(self):
        self.rows = 0
        self.columns = 0
        self.update()

    def update(self):
        if system() == 'Windows':
            self.rows, self.columns = self.getTerminalSizeWindows()
        else:
            self.rows, self.columns = os.popen('stty size', 'r').read().split()

# to be honest I found this function on internet because Windows Terminals are NIGHTMARES to deal with so yeah I cheated just for this function sorry :(
    def getTerminalSizeWindows(self):
        from ctypes import windll, create_string_buffer
        h = windll.kernel32.GetStdHandle(-12)
        csbi = create_string_buffer(22)
        res = windll.kernel32.GetConsoleScreenBufferInfo(h, csbi)
        if res:
            (bufx, bufy, curx, cury, wattr,
             left, top, right, bottom,
             maxx, maxy) = struct.unpack("hhhhHhhhhhh", csbi.raw)
            sizex = right - left + 1
            sizey = bottom - top + 1
            return sizex, sizey

    def getTerminalSize(self):
        return self.rows, self.columns

    def clearTerminal(self, stdscr):
        stdscr.clear()
