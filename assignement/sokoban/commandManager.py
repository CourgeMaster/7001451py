from gameManager import GameManager
from mapManager import MapManager
from setting import ConfigManager

class CommandManager:
    def __init__(self):
        self.mapPath = "./maps/"
        self.player = input("Enter your name: ")
        self.cm = ConfigManager()
        self.d = self.cm.getInfo()
        print("Hi " + self.player + "!")
        self.mapManager = MapManager(self.mapPath)
        self.playerMap = self.getMap()

    def getMap(self):
        if self.player in self.d:
            return int(self.d[self.player]) - 1
        return 0

    def showHelp(self):
        print("<play> : play sokoban\n<quit> : quit the program\n<player list> : print the player list\n<my save> : print your curent level\n<reset> : reset your level\n<show maps> : get the maps names\n<set map folder> : change the folder where the maps are stored (default is ./maps/)\n<rules> : print the rules of the sokoban\n<end me> : delete your player name")

    def startGame(self):
        if len(self.mapManager.getMapList()) <= 0:
            print("<ERROR> : the map folder is empty")
            return True
        gm = GameManager(self.player, self.mapManager.getMapList(), self.playerMap, self.mapManager.getPath())
        map = gm.gameLoop()
        map = map + 1
        self.d[self.player] = str(map)
        gm.resetTerm()
        return False

    def showPlayerList(self):
        for a in self.d:
            print(a, ":", self.d[a])

    def showPlayerSave(self):
        if self.player in self.d:
            print(self.d[self.player])
        else:
            print("No data on your name")

    def resetPlayerSave(self):
        if self.player in self.d:
            self.d[self.player] = "1"
            print("Back to level 1!")
        else:
            print("No data on your name")

    def printRules(self):
        print("")
        with open("./.rules", 'r+') as f:
            lines = f.readlines()
            for i in range(0, len(lines)):
                line = lines[i]
                print(line, end=" ")

    def showMaps(self):
        self.mapManager.printMaps()

    def showWrongCmd(self):
        print("<ERROR> : wrong command, type help to get the command list")

    def changeMapFolder(self):
        self.mapManager.changePath()

    def endMe(self):
        ans = input("Are you sure about that ? ")
        if ans.upper() == "YES":
            if self.player in self.d:
                del self.d[self.player]
                print("you have been deleted")
            else:
                print("No data on your name")
                return True
            return False
        elif ans.upper() == "NO":
            print("ok")
            return True
        else:
            print("I didn't understand")
            self.endMe()

    #this is a big 'if' forest
    def cmdHandler(self, cmd):
        loop = True
        if cmd == "play":
            loop = self.startGame()
        elif cmd == "help":
            self.showHelp()
        elif cmd == "player list":
            self.showPlayerList()
        elif cmd == "my save":
            self.showPlayerSave()
        elif cmd == "reset":
            self.resetPlayerSave()
        elif cmd == "show maps":
            self.showMaps()
        elif cmd == "set map folder":
            self.changeMapFolder()
        elif cmd == "rules":
            self.printRules()
        elif cmd == "end me":
            loop = self.endMe()
        elif cmd == "quit":
            loop = False
        else:
            self.showWrongCmd()
        return loop

    def commandLoop(self):
        cmd = input("Enter a command (type help to get the command list): ")
        if self.cmdHandler(cmd) == True:
            self.commandLoop()

        self.cm.writeInfo(self.d)
