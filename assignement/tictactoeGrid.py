import sys


##
# this print a 3 X 3 grid filled with  2 X 1 cells
#
def standard():
    str1 = "+--+--+--+"
    str2 = "|  |  |  |"

    i = 0
    while i != 7:
        i = i + 1
        if i % 2 == 0:
            print(str2)
        else:
            print(str1)

##
# this create the pattern that will be repeated
#
def createPattern(width, col):
    tmp1 = "+"
    tmp2 = "|"
    i = 0
    while i != width:
        tmp1 = tmp1 + "-"
        tmp2 = tmp2 + " "
        i = i + 1
    final1 = ""
    final2 = ""
    i = 0
    while i != col:
        final1 = final1 + tmp1
        final2 = final2 + tmp2
        i = i + 1
    final1 = final1 + "+"
    final2 = final2 + "|"
    return final1, final2

##
# this create a custom grid with custom cells
#
def smart():
    height = int(input("enter the height of a cell = "))
    width = int(input("enter the width of a cell = "))
    col = int(input("enter the number of a columns = "))
    row = int(input("enter the number of a rows = "))
    if height <= 0 or width <= 0 or col <= 0 or row <= 0:
        return
    final1, final2 = createPattern(width, col)
    print(final1)
    i = 0
    while i != row:
        j = 0
        while j != height:
            print(final2)
            j = j + 1
        print(final1)
        i = i + 1

if len(sys.argv) > 1 and sys.argv[1] == "smart":
    smart()
else:
    standard()
