
def verify(str):
    hasUpper = False
    hasLower = False
    hasDigit = False
    i = 0
    while i != len(str):
        tmp = str[i];
        if tmp.isupper():
            hasUpper = True
        if tmp.islower():
            hasLower = True
        if tmp.isdigit():
            hasDigit = True
        i = i + 1
    if len(str) < 8 or hasUpper == False or hasLower == False or hasDigit == False:
        return -1
    return 0

def main():
    passw = input("password: ")
    if verify(passw) == -1:
        print("wrong")
        main()
    newpass = input("password: ")
    while passw != newpass:
        print("wrong")
        newpass = input("password: ")
    print("ok")
main()
