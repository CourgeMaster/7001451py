def checkWavelenght(value):
    if value > 1E-01:
        print("Radio Waves")
    elif value < 1E-01 and value > 1E-03:
        print("Microwaves")
    elif value < 1E-03 and value > 7E-07:
        print("Infrared")
    elif value < 7E-07 and value > 4E-07:
        print("Visible Light")
    elif value < 4E-07 and value > 1E-08:
        print("Ultraviolet")
    elif value < 1E-08 and value > 1E-11:
        print("X-Rays")
    elif value < 1E-11:
        print("Gamma rays")

def checkFrequency(value):
    if value < 3E9:
        print("Radio Waves")
    elif value < 3E11 and value > 3E9:
        print("Microwaves")
    elif value < 4E14 and value > 3E-11:
        print("Infrared")
    elif value < 7.5E14 and value > 4E14:
        print("Visible Light")
    elif value < 3E16 and value > 7.5E14:
        print("Ultraviolet")
    elif value < 3E19 and value > 3E16:
        print("X-Rays")
    elif value > 3E19:
        print("Gamma rays")

type = input("Enter type ")
value = float(input("Enter value "))

if type == "W":
    checkWavelenght(value)
elif type == "F":
    checkFrequency(value)
else:
    print("Error: Wrong Type")
