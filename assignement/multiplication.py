i = 1
while i <= 10:
    j = 1
    while j <= 10:
        if i * j < 10:
            print(" ", end="")
        if i * j != 100:
            print(" ", end="")
        print(i * j, " ", end="")
        j = j + 1
    print("")
    i = i + 1
