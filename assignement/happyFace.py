from ezgraphics import GraphicsWindow

class Circle:
    def __init__(self, x, y, w, h, color, outColor):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.color = color
        self.outColor = outColor

    def draw(self, canvas):
        canvas.setOutline(self.outColor)
        canvas.setFill(self.color)
        canvas.drawOval(self.x, self.y, self.w, self.h)

class Triangle:
    def __init__(self, x1, y1, x2, y2, x3, y3, color, outColor):
        self.p1 = (x1, y1)
        self.p2 = (x2, y2)
        self.p3 = (x3, y3)
        self.color = color
        self.outColor = outColor

    def draw(self, canvas):
        canvas.setOutline(self.outColor)
        canvas.setFill(self.color)
        canvas.drawPoly(self.p1[0], self.p1[1], self.p2[0], self.p2[1], self.p3[0], self.p3[1])

def main():
    win = GraphicsWindow(640, 480)
    win.setTitle("UN/IX Highway To Shell");
    canvas = win.canvas()
    head = Circle(10, 10, 250, 300, "white", "gold")
    head.draw(canvas)
    leftEye = Circle(50, 100, 60, 40, "yellow", "gold")
    leftEye.draw(canvas)
    leftEye2 = Circle(70, 110, 20, 20, "black", "black")
    leftEye2.draw(canvas)
    rightEye = Circle(165, 100, 60, 40, "yellow", "gold")
    rightEye.draw(canvas)
    rightEye2 = Circle(185, 110, 20, 20, "black", "black")
    rightEye2.draw(canvas)
    mouth = Circle(80, 220, 100, 40, "yellow", "gold")
    mouth.draw(canvas)
    nose = Triangle(130, 140, 110, 200, 150, 200, "yellow", "gold")
    nose.draw(canvas)
    win.wait()

main()
