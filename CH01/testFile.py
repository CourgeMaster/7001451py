##
# print str on the standard output
#

def printThings(str):
    print(str)

def loop():
    str = ""
    while (str != "exit"):
        str = input()
        printThings(str)

# Testing different types in the same variable
taxRate = 5  # int
print(taxRate)
taxRate = 5.5  # float
print(taxRate)
taxRate = "Non-taxable" # string
print(taxRate)
print(taxRate + "??")
