from random import random

TRIES = 100000
hits = 0

for i in range(TRIES) :
    f = random();
    x = -1 + 2 * f
    f = random();
    y = -1 + 2 * f
    if x * x + y * y <= 1:
        hits = hits + 1
pi = 4.0 * hits / TRIES
print(pi)
